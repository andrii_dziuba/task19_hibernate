package service;


import dao.AuthorBookRelationDAO;
import dao.BooksDAO;
import model.Books;

import java.util.Iterator;
import java.util.List;

public class BooksService {

    private BooksDAO booksDAO = BooksDAO.getInstance();

    private AuthorBookRelationDAO relationDAO = AuthorBookRelationDAO.getInstance();

    public List<Books> getAllBooks() {
        return booksDAO.listBooks();
    }

    public Books getBookByID(int id) {
        Iterator it = getAllBooks().iterator();
        while(it.hasNext()) {
            Books book = (Books) it.next();
            if(book.getId() == id) return book;
        }
        return null;
    }

    public void save(Books b) {
        if(b.getId() == -1) {
            booksDAO.addBook(b);
        } else {
            booksDAO.updateBook(b.getId(), b.getBookName(), b.getGenre(), b.getPublished(), b.getRating());
        }
    }

    public void delete(int bookId) {
//        relationDAO.deleteAllBookAuthors(bookId); // TODO: Replace with service
        booksDAO.deleteBook(bookId);
    }

}
