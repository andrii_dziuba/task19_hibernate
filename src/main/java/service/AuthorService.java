package service;


import dao.AuthorBookRelationDAO;
import dao.AuthorDAO;
import model.Author;

import java.util.Iterator;
import java.util.List;

public class AuthorService {

    private AuthorDAO authorDAO = AuthorDAO.getInstance();

    private AuthorBookRelationDAO relationDAO = AuthorBookRelationDAO.getInstance();

    public List<Author> getAllAuthors() {
        return authorDAO.listAuthor();
    }

    public Author getAuthorByID(int id) {
        Iterator it = getAllAuthors().iterator();
        while(it.hasNext()) {
            Author author = (Author) it.next();
            if(author.getId() == id) return author;
        }
        return null;
    }

    public boolean save(Author a) {
        if(a.getId() == -1) {
            for(Author _a : getAllAuthors())
                if(_a.equals(a)) return false;
            authorDAO.addAuthor(a);
        } else {
            for(Author _a : getAllAuthors()) if(_a.equalsWithId(a)) return false;
            authorDAO.updateAuthor(a.getId(), a.getAuthorName(), a.getGender(), a.getBorn());
        }
        return true;
    }

    public void delete(int authorId) {
//        relationDAO.deleteAllAuthorBooks(authorId); // TODO: Replace with service
        authorDAO.deleteAuthor(authorId);
    }
}
