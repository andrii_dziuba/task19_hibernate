package config;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DefaultSessionFactory {

    private static SessionFactory sessionFactory;

    static {
        try { // Create the SessionFactory from hibernate.cfg.xml
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void close() {
        sessionFactory.close();
    }
}
