import config.DefaultSessionFactory;
import model.Author;
import service.AuthorService;
import service.BooksService;

import java.sql.Date;
import java.time.LocalDate;

public class Application {

    AuthorService authorService = new AuthorService();
    BooksService booksService = new BooksService();

    Application() {
        System.out.println(authorService.getAllAuthors());
        Author a = new Author("Dodo", "male", Date.valueOf(LocalDate.now()));
        System.out.println(a);
        authorService.save(a);
    }

    public static void main(String[] args) {
        new Application();

        DefaultSessionFactory.close();
    }
}
